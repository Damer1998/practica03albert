import 'package:flutter/material.dart';
import 'package:proyecto13tarea/src/pages/home_page.dart';
import 'package:proyecto13tarea/src/pages/alert_page.dart';
import 'package:proyecto13tarea/src/pages/avatar_page.dart';
import 'package:proyecto13tarea/src/pages/card_page.dart';

import 'package:proyecto13tarea/src/pages/animated_page.dart';
import 'package:proyecto13tarea/src/pages/inputs_page.dart';
import 'package:proyecto13tarea/src/pages/slider_page.dart';
import 'package:proyecto13tarea/src/pages/lista_page.dart';
//import 'package:proyecto13/src/pages/hometemp.dart';  lista_page

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      // home: HomePage(),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        'alert': (context) => AlertPage(),
        'avatar': (context) => AvatarPage(),
        'card': (context) => CardPage(),
        'animated': (context) => AnimatedPage(),
        'inputs': (context) => InputsPage(),
        'slide': (context) => SliderPage(),
        'lista': (context) => ListaPage(),
      },
    );
  }
}
