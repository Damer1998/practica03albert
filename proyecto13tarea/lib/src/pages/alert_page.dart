// Sesion Alert
import 'package:flutter/material.dart';

// Perteneciente a la clase home_temp.dart’

// Clase AlertPage
class AlertPage extends StatelessWidget {
  final options = [
    'Lunes a Viernes',
    'Lunes a Domingo',
    'Lunes a Miercoles',
    'Sabado a Domingo',
    'Lunes - Martes - Jueves'
  ];

  // TextStyle estilo = TextStyle(color : Colors.green , fontSize: 25.0);

  List<Widget> _crearItemsCortal() {
    return options.map((item) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item),
            subtitle: Text('6:00 a.m - 6:15 p.m'),
            onTap: () {
              print("Alarma establecida");
            },
          ),
        ],
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Programar Alarma'),
        backgroundColor: Colors.brown[800],
      ),
      body: ListView(children: _crearItemsCortal()),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context);
        },
        label: Text('Regresar'),
        icon: Icon(Icons.arrow_forward_ios),
        backgroundColor: Colors.blueAccent,
      ),
    );
  }
}
