import 'package:flutter/material.dart';

// Perteneciente a la clase home_temp.dart’ 

// Reemplazo de Home_page
class HomePageTemp extends StatelessWidget {


 @override
Widget build(BuildContext context) {
  return new ListView(
    children: ListTile.divideTiles( 
      context: context,
      tiles: 
    [

      ListTile(
        leading : Icon(Icons.add_alert),
        title: Text("Alertas", style: TextStyle(fontSize: 20 , color: Colors.red[900] ,    ) , ),
        subtitle: Text("Desea programar una notificacion"),
        trailing: Icon(Icons.keyboard_arrow_right),

        onTap: (){
          print("Alert");
        },
      ),

            ListTile(
        leading : Icon(Icons.accessibility),
        title: Text("Avatars", style: TextStyle(fontSize: 20 , color: Colors.red[900] ,    ) , ),
        subtitle: Text("Desea crear un nuevo perfil"),
        trailing: Icon(Icons.keyboard_arrow_right),

                onTap: (){
          print("Avatars");
        },
      ),

            ListTile(
        leading : Icon(Icons.assignment),
        title: Text("Cards - Tarjetas", style: TextStyle(fontSize: 20 , color: Colors.red[900] ,    ) , ),
        subtitle: Text("Desea confirgurar tarjetas"),
        trailing: Icon(Icons.keyboard_arrow_right),

                onTap: (){
          print("Cardss");
        },
      ),

            ListTile(
        leading : Icon(Icons.data_usage),
        title: Text("Animated Container", style: TextStyle(fontSize: 20 , color: Colors.red[900] ,    ) , ),
        subtitle: Text("Desea establecer una animacion"),
        trailing: Icon(Icons.keyboard_arrow_right),

                onTap: (){
          print("Animated");
        },
      ),

            ListTile(
        leading : Icon(Icons.blur_off),
        title: Text("Inputs", style: TextStyle(fontSize: 20 , color: Colors.red[900] ,    ) , ),
        subtitle: Text("Desea ingresar Datos"),
        trailing: Icon(Icons.keyboard_arrow_right),
        
                onTap: (){
          print("Inputs");
        },
      ),

            ListTile(
        leading : Icon(Icons.format_list_bulleted),
        
        title: Text("Listas y Scroll", style: TextStyle(fontSize: 20 , color: Colors.red[900] ,    ) , ),
       subtitle: Text("Desee ingresar a la lista"),
        trailing: Icon(Icons.keyboard_arrow_right),

        
        
                onTap: (){
          print("List");
        },
      ),


    ],
).toList(),
      );




  
}

}

