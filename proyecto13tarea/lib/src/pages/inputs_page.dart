// Seccion Inputs
import 'package:flutter/material.dart';

// Perteneciente a la clase home_temp.dart’

// Clase AlertPage
class InputsPage extends StatelessWidget {
  final options = [
    'Comunicacion',
    'Religion',
    'Matematicas',
    'Fisica',
    'Quimica'
  ];

  // TextStyle estilo = TextStyle(color : Colors.green , fontSize: 25.0);

  List<Widget> _crearItemsCortal() {
    return options.map((item) {
      return Column(
        children: <Widget>[
          ListTile(title: Text(item), subtitle: Text('Cualquier Cosa')),
        ],
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Seccion de Inputs'),
        backgroundColor: Colors.brown[800],
      ),
      body: ListView(children: _crearItemsCortal()),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context);
        },
        label: Text('Regresar'),
        icon: Icon(Icons.arrow_forward_ios),
        backgroundColor: Colors.blueAccent,
      ),
    );
  }
}
