import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'add_alert': Icons.add_alert,
  'accessibility': Icons.accessibility,
  'folder_open': Icons.folder_open,
  'data_usage': Icons.data_usage,
  'blur_off': Icons.blur_off,
  'assignment': Icons.assignment,
  'format_list_bulleted': Icons.format_list_bulleted,
};

Icon getIcon(String nombreIcono) {
  return Icon(_icons[nombreIcono]);
}
